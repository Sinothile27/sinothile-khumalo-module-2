// Write a basic program that stores and then prints
// the following data: Your name, favorite app, and city

import 'dart:io';

void main(List<String> args) {
  var myName = "Sinothile Princeton Khumalo";
  var myFavoriteApp = "YouTube";
  var myCity = "Durban";

  print("My name is $myName," +
      " " +
      "my favorite app is $myFavoriteApp" +
      " " +
      "and my city is $myCity");

  print(
      "........................................................................................");

  print(
      "Hello MTN, I am preparing for the change...Please enter your details below (^_^)");

  print(
      "........................................................................................");

  stdout.writeln("Please enter your name: ");
  var name = stdin.readLineSync().toString();

  stdout.writeln("Please enter your favourite app: ");
  var favouriteApp = stdin.readLineSync().toString();

  stdout.writeln("Please enter your city: ");
  var city = stdin.readLineSync().toString();

  print("Your name is $name " +
      ", your favorite App is $favouriteApp " +
      "and the name of your city is $city.");
}
