// Create an array to store all the winning apps of the MTN Business App of the Year Awards since 2012;
//a) Sort and print the apps by name;
//b) Print the winning app of 2017 and the winning app of 2018.;
//c) then Print total number of apps from the array.

void main(List<String> args) {
  Set winningApps = {
    'Ambani',
    'EasyEquities',
    'Naked Insurance',
    'Khula',
    'Standard Bank Shyft',
    'Domestly',
    'WumDrop',
    'Live Inspect',
    'SnapScan',
    'FNB Banking'
  };

  // a) Sorting and printing the app by name
  List appNameList = winningApps.toList();
  appNameList.sort();

  print(
      "The winning apps of the MTN Business App of the Year Awards since 2012 are as follows:");
  print(
      "------------------------------------------------------------------------------------------------------------------------------");
  for (var _appNameList in appNameList) {
    print(_appNameList);
  }

  // b) Printing the winning app of 2017 and the winning app of 2018
  print(
      "-------------------------------------------------------------------------------------------------------------------------------");

  print("The winning apps of the MTN Business App of the Year 2017 " +
      appNameList[8]);
  print("The winning apps of the MTN Business App of the Year 2018 " +
      appNameList[4]);

  // c) Print total number of apps from the array
  print(
      "-------------------------------------------------------------------------------------------------------------------------------dra");
  print("The total number of apps in the Array collection: " +
      appNameList.length.toString());
}
